/*
rtsp-simple-server API

API of rtsp-simple-server, a server and proxy that supports various protocols.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package rtsp_simple_server

import (
	"encoding/json"
)

// PathReadersItem struct for PathReadersItem
type PathReadersItem struct {
	Type string `json:"type"`
	Id *string `json:"id,omitempty"`
}

// NewPathReadersItem instantiates a new PathReadersItem object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPathReadersItem(type_ string) *PathReadersItem {
	this := PathReadersItem{}
	this.Type = type_
	return &this
}

// NewPathReadersItemWithDefaults instantiates a new PathReadersItem object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPathReadersItemWithDefaults() *PathReadersItem {
	this := PathReadersItem{}
	return &this
}

// GetType returns the Type field value
func (o *PathReadersItem) GetType() string {
	if o == nil {
		var ret string
		return ret
	}

	return o.Type
}

// GetTypeOk returns a tuple with the Type field value
// and a boolean to check if the value has been set.
func (o *PathReadersItem) GetTypeOk() (*string, bool) {
	if o == nil  {
		return nil, false
	}
	return &o.Type, true
}

// SetType sets field value
func (o *PathReadersItem) SetType(v string) {
	o.Type = v
}

// GetId returns the Id field value if set, zero value otherwise.
func (o *PathReadersItem) GetId() string {
	if o == nil || o.Id == nil {
		var ret string
		return ret
	}
	return *o.Id
}

// GetIdOk returns a tuple with the Id field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *PathReadersItem) GetIdOk() (*string, bool) {
	if o == nil || o.Id == nil {
		return nil, false
	}
	return o.Id, true
}

// HasId returns a boolean if a field has been set.
func (o *PathReadersItem) HasId() bool {
	if o != nil && o.Id != nil {
		return true
	}

	return false
}

// SetId gets a reference to the given string and assigns it to the Id field.
func (o *PathReadersItem) SetId(v string) {
	o.Id = &v
}

func (o PathReadersItem) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if true {
		toSerialize["type"] = o.Type
	}
	if o.Id != nil {
		toSerialize["id"] = o.Id
	}
	return json.Marshal(toSerialize)
}

type NullablePathReadersItem struct {
	value *PathReadersItem
	isSet bool
}

func (v NullablePathReadersItem) Get() *PathReadersItem {
	return v.value
}

func (v *NullablePathReadersItem) Set(val *PathReadersItem) {
	v.value = val
	v.isSet = true
}

func (v NullablePathReadersItem) IsSet() bool {
	return v.isSet
}

func (v *NullablePathReadersItem) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePathReadersItem(val *PathReadersItem) *NullablePathReadersItem {
	return &NullablePathReadersItem{value: val, isSet: true}
}

func (v NullablePathReadersItem) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePathReadersItem) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


