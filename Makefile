SHELL=/bin/bash -o pipefail

export PWD := $(shell pwd)

generate.rtsp-simple-server-client:
	docker run --rm \
      -v ${PWD}:/local openapitools/openapi-generator-cli generate \
      -i /local/hack/rtsp-simple-server.openapi.yaml \
      -g go \
      -o /local \
      --package-name rtsp_simple_server \
      --git-host gitlab.com \
      --git-user-id vtvr \
      --git-repo-id rtsp_simple_server_client