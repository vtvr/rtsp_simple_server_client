/*
rtsp-simple-server API

API of rtsp-simple-server, a server and proxy that supports various protocols.

API version: 1.0.0
*/

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package rtsp_simple_server

import (
	"encoding/json"
)

// Path struct for Path
type Path struct {
	ConfName *string `json:"confName,omitempty"`
	Conf *PathConf `json:"conf,omitempty"`
	Source *PathSource `json:"source,omitempty"`
	SourceReady *bool `json:"sourceReady,omitempty"`
	Readers *[]PathReadersItem `json:"readers,omitempty"`
}

// NewPath instantiates a new Path object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewPath() *Path {
	this := Path{}
	return &this
}

// NewPathWithDefaults instantiates a new Path object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewPathWithDefaults() *Path {
	this := Path{}
	return &this
}

// GetConfName returns the ConfName field value if set, zero value otherwise.
func (o *Path) GetConfName() string {
	if o == nil || o.ConfName == nil {
		var ret string
		return ret
	}
	return *o.ConfName
}

// GetConfNameOk returns a tuple with the ConfName field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Path) GetConfNameOk() (*string, bool) {
	if o == nil || o.ConfName == nil {
		return nil, false
	}
	return o.ConfName, true
}

// HasConfName returns a boolean if a field has been set.
func (o *Path) HasConfName() bool {
	if o != nil && o.ConfName != nil {
		return true
	}

	return false
}

// SetConfName gets a reference to the given string and assigns it to the ConfName field.
func (o *Path) SetConfName(v string) {
	o.ConfName = &v
}

// GetConf returns the Conf field value if set, zero value otherwise.
func (o *Path) GetConf() PathConf {
	if o == nil || o.Conf == nil {
		var ret PathConf
		return ret
	}
	return *o.Conf
}

// GetConfOk returns a tuple with the Conf field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Path) GetConfOk() (*PathConf, bool) {
	if o == nil || o.Conf == nil {
		return nil, false
	}
	return o.Conf, true
}

// HasConf returns a boolean if a field has been set.
func (o *Path) HasConf() bool {
	if o != nil && o.Conf != nil {
		return true
	}

	return false
}

// SetConf gets a reference to the given PathConf and assigns it to the Conf field.
func (o *Path) SetConf(v PathConf) {
	o.Conf = &v
}

// GetSource returns the Source field value if set, zero value otherwise.
func (o *Path) GetSource() PathSource {
	if o == nil || o.Source == nil {
		var ret PathSource
		return ret
	}
	return *o.Source
}

// GetSourceOk returns a tuple with the Source field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Path) GetSourceOk() (*PathSource, bool) {
	if o == nil || o.Source == nil {
		return nil, false
	}
	return o.Source, true
}

// HasSource returns a boolean if a field has been set.
func (o *Path) HasSource() bool {
	if o != nil && o.Source != nil {
		return true
	}

	return false
}

// SetSource gets a reference to the given PathSource and assigns it to the Source field.
func (o *Path) SetSource(v PathSource) {
	o.Source = &v
}

// GetSourceReady returns the SourceReady field value if set, zero value otherwise.
func (o *Path) GetSourceReady() bool {
	if o == nil || o.SourceReady == nil {
		var ret bool
		return ret
	}
	return *o.SourceReady
}

// GetSourceReadyOk returns a tuple with the SourceReady field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Path) GetSourceReadyOk() (*bool, bool) {
	if o == nil || o.SourceReady == nil {
		return nil, false
	}
	return o.SourceReady, true
}

// HasSourceReady returns a boolean if a field has been set.
func (o *Path) HasSourceReady() bool {
	if o != nil && o.SourceReady != nil {
		return true
	}

	return false
}

// SetSourceReady gets a reference to the given bool and assigns it to the SourceReady field.
func (o *Path) SetSourceReady(v bool) {
	o.SourceReady = &v
}

// GetReaders returns the Readers field value if set, zero value otherwise.
func (o *Path) GetReaders() []PathReadersItem {
	if o == nil || o.Readers == nil {
		var ret []PathReadersItem
		return ret
	}
	return *o.Readers
}

// GetReadersOk returns a tuple with the Readers field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *Path) GetReadersOk() (*[]PathReadersItem, bool) {
	if o == nil || o.Readers == nil {
		return nil, false
	}
	return o.Readers, true
}

// HasReaders returns a boolean if a field has been set.
func (o *Path) HasReaders() bool {
	if o != nil && o.Readers != nil {
		return true
	}

	return false
}

// SetReaders gets a reference to the given []PathReadersItem and assigns it to the Readers field.
func (o *Path) SetReaders(v []PathReadersItem) {
	o.Readers = &v
}

func (o Path) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.ConfName != nil {
		toSerialize["confName"] = o.ConfName
	}
	if o.Conf != nil {
		toSerialize["conf"] = o.Conf
	}
	if o.Source != nil {
		toSerialize["source"] = o.Source
	}
	if o.SourceReady != nil {
		toSerialize["sourceReady"] = o.SourceReady
	}
	if o.Readers != nil {
		toSerialize["readers"] = o.Readers
	}
	return json.Marshal(toSerialize)
}

type NullablePath struct {
	value *Path
	isSet bool
}

func (v NullablePath) Get() *Path {
	return v.value
}

func (v *NullablePath) Set(val *Path) {
	v.value = val
	v.isSet = true
}

func (v NullablePath) IsSet() bool {
	return v.isSet
}

func (v *NullablePath) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullablePath(val *Path) *NullablePath {
	return &NullablePath{value: val, isSet: true}
}

func (v NullablePath) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullablePath) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


