# RTSPSession

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RemoteAddr** | Pointer to **string** |  | [optional] 
**State** | Pointer to **string** |  | [optional] 

## Methods

### NewRTSPSession

`func NewRTSPSession() *RTSPSession`

NewRTSPSession instantiates a new RTSPSession object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewRTSPSessionWithDefaults

`func NewRTSPSessionWithDefaults() *RTSPSession`

NewRTSPSessionWithDefaults instantiates a new RTSPSession object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetRemoteAddr

`func (o *RTSPSession) GetRemoteAddr() string`

GetRemoteAddr returns the RemoteAddr field if non-nil, zero value otherwise.

### GetRemoteAddrOk

`func (o *RTSPSession) GetRemoteAddrOk() (*string, bool)`

GetRemoteAddrOk returns a tuple with the RemoteAddr field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRemoteAddr

`func (o *RTSPSession) SetRemoteAddr(v string)`

SetRemoteAddr sets RemoteAddr field to given value.

### HasRemoteAddr

`func (o *RTSPSession) HasRemoteAddr() bool`

HasRemoteAddr returns a boolean if a field has been set.

### GetState

`func (o *RTSPSession) GetState() string`

GetState returns the State field if non-nil, zero value otherwise.

### GetStateOk

`func (o *RTSPSession) GetStateOk() (*string, bool)`

GetStateOk returns a tuple with the State field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetState

`func (o *RTSPSession) SetState(v string)`

SetState sets State field to given value.

### HasState

`func (o *RTSPSession) HasState() bool`

HasState returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


