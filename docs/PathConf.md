# PathConf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Source** | Pointer to **string** |  | [optional] 
**SourceProtocol** | Pointer to **string** |  | [optional] 
**SourceAnyPortEnable** | Pointer to **bool** |  | [optional] 
**SourceFingerprint** | Pointer to **string** |  | [optional] 
**SourceOnDemand** | Pointer to **bool** |  | [optional] 
**SourceOnDemandStartTimeout** | Pointer to **string** |  | [optional] 
**SourceOnDemandCloseAfter** | Pointer to **string** |  | [optional] 
**SourceRedirect** | Pointer to **string** |  | [optional] 
**DisablePublisherOverride** | Pointer to **bool** |  | [optional] 
**Fallback** | Pointer to **string** |  | [optional] 
**PublishUser** | Pointer to **string** |  | [optional] 
**PublishPass** | Pointer to **string** |  | [optional] 
**PublishIPs** | Pointer to **[]string** |  | [optional] 
**ReadUser** | Pointer to **string** |  | [optional] 
**ReadPass** | Pointer to **string** |  | [optional] 
**ReadIPs** | Pointer to **[]string** |  | [optional] 
**RunOnInit** | Pointer to **string** |  | [optional] 
**RunOnInitRestart** | Pointer to **bool** |  | [optional] 
**RunOnDemand** | Pointer to **string** |  | [optional] 
**RunOnDemandRestart** | Pointer to **bool** |  | [optional] 
**RunOnDemandStartTimeout** | Pointer to **string** |  | [optional] 
**RunOnDemandCloseAfter** | Pointer to **string** |  | [optional] 
**PingOnPublish** | Pointer to **string** |  | [optional] 
**PingOnPublishStop** | Pointer to **string** |  | [optional] 
**RunOnPublish** | Pointer to **string** |  | [optional] 
**RunOnPublishRestart** | Pointer to **bool** |  | [optional] 
**RunOnRead** | Pointer to **string** |  | [optional] 
**RunOnReadRestart** | Pointer to **bool** |  | [optional] 

## Methods

### NewPathConf

`func NewPathConf() *PathConf`

NewPathConf instantiates a new PathConf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPathConfWithDefaults

`func NewPathConfWithDefaults() *PathConf`

NewPathConfWithDefaults instantiates a new PathConf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSource

`func (o *PathConf) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *PathConf) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *PathConf) SetSource(v string)`

SetSource sets Source field to given value.

### HasSource

`func (o *PathConf) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetSourceProtocol

`func (o *PathConf) GetSourceProtocol() string`

GetSourceProtocol returns the SourceProtocol field if non-nil, zero value otherwise.

### GetSourceProtocolOk

`func (o *PathConf) GetSourceProtocolOk() (*string, bool)`

GetSourceProtocolOk returns a tuple with the SourceProtocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceProtocol

`func (o *PathConf) SetSourceProtocol(v string)`

SetSourceProtocol sets SourceProtocol field to given value.

### HasSourceProtocol

`func (o *PathConf) HasSourceProtocol() bool`

HasSourceProtocol returns a boolean if a field has been set.

### GetSourceAnyPortEnable

`func (o *PathConf) GetSourceAnyPortEnable() bool`

GetSourceAnyPortEnable returns the SourceAnyPortEnable field if non-nil, zero value otherwise.

### GetSourceAnyPortEnableOk

`func (o *PathConf) GetSourceAnyPortEnableOk() (*bool, bool)`

GetSourceAnyPortEnableOk returns a tuple with the SourceAnyPortEnable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceAnyPortEnable

`func (o *PathConf) SetSourceAnyPortEnable(v bool)`

SetSourceAnyPortEnable sets SourceAnyPortEnable field to given value.

### HasSourceAnyPortEnable

`func (o *PathConf) HasSourceAnyPortEnable() bool`

HasSourceAnyPortEnable returns a boolean if a field has been set.

### GetSourceFingerprint

`func (o *PathConf) GetSourceFingerprint() string`

GetSourceFingerprint returns the SourceFingerprint field if non-nil, zero value otherwise.

### GetSourceFingerprintOk

`func (o *PathConf) GetSourceFingerprintOk() (*string, bool)`

GetSourceFingerprintOk returns a tuple with the SourceFingerprint field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceFingerprint

`func (o *PathConf) SetSourceFingerprint(v string)`

SetSourceFingerprint sets SourceFingerprint field to given value.

### HasSourceFingerprint

`func (o *PathConf) HasSourceFingerprint() bool`

HasSourceFingerprint returns a boolean if a field has been set.

### GetSourceOnDemand

`func (o *PathConf) GetSourceOnDemand() bool`

GetSourceOnDemand returns the SourceOnDemand field if non-nil, zero value otherwise.

### GetSourceOnDemandOk

`func (o *PathConf) GetSourceOnDemandOk() (*bool, bool)`

GetSourceOnDemandOk returns a tuple with the SourceOnDemand field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceOnDemand

`func (o *PathConf) SetSourceOnDemand(v bool)`

SetSourceOnDemand sets SourceOnDemand field to given value.

### HasSourceOnDemand

`func (o *PathConf) HasSourceOnDemand() bool`

HasSourceOnDemand returns a boolean if a field has been set.

### GetSourceOnDemandStartTimeout

`func (o *PathConf) GetSourceOnDemandStartTimeout() string`

GetSourceOnDemandStartTimeout returns the SourceOnDemandStartTimeout field if non-nil, zero value otherwise.

### GetSourceOnDemandStartTimeoutOk

`func (o *PathConf) GetSourceOnDemandStartTimeoutOk() (*string, bool)`

GetSourceOnDemandStartTimeoutOk returns a tuple with the SourceOnDemandStartTimeout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceOnDemandStartTimeout

`func (o *PathConf) SetSourceOnDemandStartTimeout(v string)`

SetSourceOnDemandStartTimeout sets SourceOnDemandStartTimeout field to given value.

### HasSourceOnDemandStartTimeout

`func (o *PathConf) HasSourceOnDemandStartTimeout() bool`

HasSourceOnDemandStartTimeout returns a boolean if a field has been set.

### GetSourceOnDemandCloseAfter

`func (o *PathConf) GetSourceOnDemandCloseAfter() string`

GetSourceOnDemandCloseAfter returns the SourceOnDemandCloseAfter field if non-nil, zero value otherwise.

### GetSourceOnDemandCloseAfterOk

`func (o *PathConf) GetSourceOnDemandCloseAfterOk() (*string, bool)`

GetSourceOnDemandCloseAfterOk returns a tuple with the SourceOnDemandCloseAfter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceOnDemandCloseAfter

`func (o *PathConf) SetSourceOnDemandCloseAfter(v string)`

SetSourceOnDemandCloseAfter sets SourceOnDemandCloseAfter field to given value.

### HasSourceOnDemandCloseAfter

`func (o *PathConf) HasSourceOnDemandCloseAfter() bool`

HasSourceOnDemandCloseAfter returns a boolean if a field has been set.

### GetSourceRedirect

`func (o *PathConf) GetSourceRedirect() string`

GetSourceRedirect returns the SourceRedirect field if non-nil, zero value otherwise.

### GetSourceRedirectOk

`func (o *PathConf) GetSourceRedirectOk() (*string, bool)`

GetSourceRedirectOk returns a tuple with the SourceRedirect field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceRedirect

`func (o *PathConf) SetSourceRedirect(v string)`

SetSourceRedirect sets SourceRedirect field to given value.

### HasSourceRedirect

`func (o *PathConf) HasSourceRedirect() bool`

HasSourceRedirect returns a boolean if a field has been set.

### GetDisablePublisherOverride

`func (o *PathConf) GetDisablePublisherOverride() bool`

GetDisablePublisherOverride returns the DisablePublisherOverride field if non-nil, zero value otherwise.

### GetDisablePublisherOverrideOk

`func (o *PathConf) GetDisablePublisherOverrideOk() (*bool, bool)`

GetDisablePublisherOverrideOk returns a tuple with the DisablePublisherOverride field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDisablePublisherOverride

`func (o *PathConf) SetDisablePublisherOverride(v bool)`

SetDisablePublisherOverride sets DisablePublisherOverride field to given value.

### HasDisablePublisherOverride

`func (o *PathConf) HasDisablePublisherOverride() bool`

HasDisablePublisherOverride returns a boolean if a field has been set.

### GetFallback

`func (o *PathConf) GetFallback() string`

GetFallback returns the Fallback field if non-nil, zero value otherwise.

### GetFallbackOk

`func (o *PathConf) GetFallbackOk() (*string, bool)`

GetFallbackOk returns a tuple with the Fallback field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFallback

`func (o *PathConf) SetFallback(v string)`

SetFallback sets Fallback field to given value.

### HasFallback

`func (o *PathConf) HasFallback() bool`

HasFallback returns a boolean if a field has been set.

### GetPublishUser

`func (o *PathConf) GetPublishUser() string`

GetPublishUser returns the PublishUser field if non-nil, zero value otherwise.

### GetPublishUserOk

`func (o *PathConf) GetPublishUserOk() (*string, bool)`

GetPublishUserOk returns a tuple with the PublishUser field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublishUser

`func (o *PathConf) SetPublishUser(v string)`

SetPublishUser sets PublishUser field to given value.

### HasPublishUser

`func (o *PathConf) HasPublishUser() bool`

HasPublishUser returns a boolean if a field has been set.

### GetPublishPass

`func (o *PathConf) GetPublishPass() string`

GetPublishPass returns the PublishPass field if non-nil, zero value otherwise.

### GetPublishPassOk

`func (o *PathConf) GetPublishPassOk() (*string, bool)`

GetPublishPassOk returns a tuple with the PublishPass field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublishPass

`func (o *PathConf) SetPublishPass(v string)`

SetPublishPass sets PublishPass field to given value.

### HasPublishPass

`func (o *PathConf) HasPublishPass() bool`

HasPublishPass returns a boolean if a field has been set.

### GetPublishIPs

`func (o *PathConf) GetPublishIPs() []string`

GetPublishIPs returns the PublishIPs field if non-nil, zero value otherwise.

### GetPublishIPsOk

`func (o *PathConf) GetPublishIPsOk() (*[]string, bool)`

GetPublishIPsOk returns a tuple with the PublishIPs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPublishIPs

`func (o *PathConf) SetPublishIPs(v []string)`

SetPublishIPs sets PublishIPs field to given value.

### HasPublishIPs

`func (o *PathConf) HasPublishIPs() bool`

HasPublishIPs returns a boolean if a field has been set.

### GetReadUser

`func (o *PathConf) GetReadUser() string`

GetReadUser returns the ReadUser field if non-nil, zero value otherwise.

### GetReadUserOk

`func (o *PathConf) GetReadUserOk() (*string, bool)`

GetReadUserOk returns a tuple with the ReadUser field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadUser

`func (o *PathConf) SetReadUser(v string)`

SetReadUser sets ReadUser field to given value.

### HasReadUser

`func (o *PathConf) HasReadUser() bool`

HasReadUser returns a boolean if a field has been set.

### GetReadPass

`func (o *PathConf) GetReadPass() string`

GetReadPass returns the ReadPass field if non-nil, zero value otherwise.

### GetReadPassOk

`func (o *PathConf) GetReadPassOk() (*string, bool)`

GetReadPassOk returns a tuple with the ReadPass field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadPass

`func (o *PathConf) SetReadPass(v string)`

SetReadPass sets ReadPass field to given value.

### HasReadPass

`func (o *PathConf) HasReadPass() bool`

HasReadPass returns a boolean if a field has been set.

### GetReadIPs

`func (o *PathConf) GetReadIPs() []string`

GetReadIPs returns the ReadIPs field if non-nil, zero value otherwise.

### GetReadIPsOk

`func (o *PathConf) GetReadIPsOk() (*[]string, bool)`

GetReadIPsOk returns a tuple with the ReadIPs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadIPs

`func (o *PathConf) SetReadIPs(v []string)`

SetReadIPs sets ReadIPs field to given value.

### HasReadIPs

`func (o *PathConf) HasReadIPs() bool`

HasReadIPs returns a boolean if a field has been set.

### GetRunOnInit

`func (o *PathConf) GetRunOnInit() string`

GetRunOnInit returns the RunOnInit field if non-nil, zero value otherwise.

### GetRunOnInitOk

`func (o *PathConf) GetRunOnInitOk() (*string, bool)`

GetRunOnInitOk returns a tuple with the RunOnInit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnInit

`func (o *PathConf) SetRunOnInit(v string)`

SetRunOnInit sets RunOnInit field to given value.

### HasRunOnInit

`func (o *PathConf) HasRunOnInit() bool`

HasRunOnInit returns a boolean if a field has been set.

### GetRunOnInitRestart

`func (o *PathConf) GetRunOnInitRestart() bool`

GetRunOnInitRestart returns the RunOnInitRestart field if non-nil, zero value otherwise.

### GetRunOnInitRestartOk

`func (o *PathConf) GetRunOnInitRestartOk() (*bool, bool)`

GetRunOnInitRestartOk returns a tuple with the RunOnInitRestart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnInitRestart

`func (o *PathConf) SetRunOnInitRestart(v bool)`

SetRunOnInitRestart sets RunOnInitRestart field to given value.

### HasRunOnInitRestart

`func (o *PathConf) HasRunOnInitRestart() bool`

HasRunOnInitRestart returns a boolean if a field has been set.

### GetRunOnDemand

`func (o *PathConf) GetRunOnDemand() string`

GetRunOnDemand returns the RunOnDemand field if non-nil, zero value otherwise.

### GetRunOnDemandOk

`func (o *PathConf) GetRunOnDemandOk() (*string, bool)`

GetRunOnDemandOk returns a tuple with the RunOnDemand field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnDemand

`func (o *PathConf) SetRunOnDemand(v string)`

SetRunOnDemand sets RunOnDemand field to given value.

### HasRunOnDemand

`func (o *PathConf) HasRunOnDemand() bool`

HasRunOnDemand returns a boolean if a field has been set.

### GetRunOnDemandRestart

`func (o *PathConf) GetRunOnDemandRestart() bool`

GetRunOnDemandRestart returns the RunOnDemandRestart field if non-nil, zero value otherwise.

### GetRunOnDemandRestartOk

`func (o *PathConf) GetRunOnDemandRestartOk() (*bool, bool)`

GetRunOnDemandRestartOk returns a tuple with the RunOnDemandRestart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnDemandRestart

`func (o *PathConf) SetRunOnDemandRestart(v bool)`

SetRunOnDemandRestart sets RunOnDemandRestart field to given value.

### HasRunOnDemandRestart

`func (o *PathConf) HasRunOnDemandRestart() bool`

HasRunOnDemandRestart returns a boolean if a field has been set.

### GetRunOnDemandStartTimeout

`func (o *PathConf) GetRunOnDemandStartTimeout() string`

GetRunOnDemandStartTimeout returns the RunOnDemandStartTimeout field if non-nil, zero value otherwise.

### GetRunOnDemandStartTimeoutOk

`func (o *PathConf) GetRunOnDemandStartTimeoutOk() (*string, bool)`

GetRunOnDemandStartTimeoutOk returns a tuple with the RunOnDemandStartTimeout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnDemandStartTimeout

`func (o *PathConf) SetRunOnDemandStartTimeout(v string)`

SetRunOnDemandStartTimeout sets RunOnDemandStartTimeout field to given value.

### HasRunOnDemandStartTimeout

`func (o *PathConf) HasRunOnDemandStartTimeout() bool`

HasRunOnDemandStartTimeout returns a boolean if a field has been set.

### GetRunOnDemandCloseAfter

`func (o *PathConf) GetRunOnDemandCloseAfter() string`

GetRunOnDemandCloseAfter returns the RunOnDemandCloseAfter field if non-nil, zero value otherwise.

### GetRunOnDemandCloseAfterOk

`func (o *PathConf) GetRunOnDemandCloseAfterOk() (*string, bool)`

GetRunOnDemandCloseAfterOk returns a tuple with the RunOnDemandCloseAfter field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnDemandCloseAfter

`func (o *PathConf) SetRunOnDemandCloseAfter(v string)`

SetRunOnDemandCloseAfter sets RunOnDemandCloseAfter field to given value.

### HasRunOnDemandCloseAfter

`func (o *PathConf) HasRunOnDemandCloseAfter() bool`

HasRunOnDemandCloseAfter returns a boolean if a field has been set.

### GetPingOnPublish

`func (o *PathConf) GetPingOnPublish() string`

GetPingOnPublish returns the PingOnPublish field if non-nil, zero value otherwise.

### GetPingOnPublishOk

`func (o *PathConf) GetPingOnPublishOk() (*string, bool)`

GetPingOnPublishOk returns a tuple with the PingOnPublish field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPingOnPublish

`func (o *PathConf) SetPingOnPublish(v string)`

SetPingOnPublish sets PingOnPublish field to given value.

### HasPingOnPublish

`func (o *PathConf) HasPingOnPublish() bool`

HasPingOnPublish returns a boolean if a field has been set.

### GetPingOnPublishStop

`func (o *PathConf) GetPingOnPublishStop() string`

GetPingOnPublishStop returns the PingOnPublishStop field if non-nil, zero value otherwise.

### GetPingOnPublishStopOk

`func (o *PathConf) GetPingOnPublishStopOk() (*string, bool)`

GetPingOnPublishStopOk returns a tuple with the PingOnPublishStop field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPingOnPublishStop

`func (o *PathConf) SetPingOnPublishStop(v string)`

SetPingOnPublishStop sets PingOnPublishStop field to given value.

### HasPingOnPublishStop

`func (o *PathConf) HasPingOnPublishStop() bool`

HasPingOnPublishStop returns a boolean if a field has been set.

### GetRunOnPublish

`func (o *PathConf) GetRunOnPublish() string`

GetRunOnPublish returns the RunOnPublish field if non-nil, zero value otherwise.

### GetRunOnPublishOk

`func (o *PathConf) GetRunOnPublishOk() (*string, bool)`

GetRunOnPublishOk returns a tuple with the RunOnPublish field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnPublish

`func (o *PathConf) SetRunOnPublish(v string)`

SetRunOnPublish sets RunOnPublish field to given value.

### HasRunOnPublish

`func (o *PathConf) HasRunOnPublish() bool`

HasRunOnPublish returns a boolean if a field has been set.

### GetRunOnPublishRestart

`func (o *PathConf) GetRunOnPublishRestart() bool`

GetRunOnPublishRestart returns the RunOnPublishRestart field if non-nil, zero value otherwise.

### GetRunOnPublishRestartOk

`func (o *PathConf) GetRunOnPublishRestartOk() (*bool, bool)`

GetRunOnPublishRestartOk returns a tuple with the RunOnPublishRestart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnPublishRestart

`func (o *PathConf) SetRunOnPublishRestart(v bool)`

SetRunOnPublishRestart sets RunOnPublishRestart field to given value.

### HasRunOnPublishRestart

`func (o *PathConf) HasRunOnPublishRestart() bool`

HasRunOnPublishRestart returns a boolean if a field has been set.

### GetRunOnRead

`func (o *PathConf) GetRunOnRead() string`

GetRunOnRead returns the RunOnRead field if non-nil, zero value otherwise.

### GetRunOnReadOk

`func (o *PathConf) GetRunOnReadOk() (*string, bool)`

GetRunOnReadOk returns a tuple with the RunOnRead field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnRead

`func (o *PathConf) SetRunOnRead(v string)`

SetRunOnRead sets RunOnRead field to given value.

### HasRunOnRead

`func (o *PathConf) HasRunOnRead() bool`

HasRunOnRead returns a boolean if a field has been set.

### GetRunOnReadRestart

`func (o *PathConf) GetRunOnReadRestart() bool`

GetRunOnReadRestart returns the RunOnReadRestart field if non-nil, zero value otherwise.

### GetRunOnReadRestartOk

`func (o *PathConf) GetRunOnReadRestartOk() (*bool, bool)`

GetRunOnReadRestartOk returns a tuple with the RunOnReadRestart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnReadRestart

`func (o *PathConf) SetRunOnReadRestart(v bool)`

SetRunOnReadRestart sets RunOnReadRestart field to given value.

### HasRunOnReadRestart

`func (o *PathConf) HasRunOnReadRestart() bool`

HasRunOnReadRestart returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


