# Path

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConfName** | Pointer to **string** |  | [optional] 
**Conf** | Pointer to [**PathConf**](PathConf.md) |  | [optional] 
**Source** | Pointer to [**PathSource**](PathSource.md) |  | [optional] 
**SourceReady** | Pointer to **bool** |  | [optional] 
**Readers** | Pointer to [**[]PathReadersItem**](PathReadersItem.md) |  | [optional] 

## Methods

### NewPath

`func NewPath() *Path`

NewPath instantiates a new Path object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPathWithDefaults

`func NewPathWithDefaults() *Path`

NewPathWithDefaults instantiates a new Path object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetConfName

`func (o *Path) GetConfName() string`

GetConfName returns the ConfName field if non-nil, zero value otherwise.

### GetConfNameOk

`func (o *Path) GetConfNameOk() (*string, bool)`

GetConfNameOk returns a tuple with the ConfName field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfName

`func (o *Path) SetConfName(v string)`

SetConfName sets ConfName field to given value.

### HasConfName

`func (o *Path) HasConfName() bool`

HasConfName returns a boolean if a field has been set.

### GetConf

`func (o *Path) GetConf() PathConf`

GetConf returns the Conf field if non-nil, zero value otherwise.

### GetConfOk

`func (o *Path) GetConfOk() (*PathConf, bool)`

GetConfOk returns a tuple with the Conf field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConf

`func (o *Path) SetConf(v PathConf)`

SetConf sets Conf field to given value.

### HasConf

`func (o *Path) HasConf() bool`

HasConf returns a boolean if a field has been set.

### GetSource

`func (o *Path) GetSource() PathSource`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *Path) GetSourceOk() (*PathSource, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *Path) SetSource(v PathSource)`

SetSource sets Source field to given value.

### HasSource

`func (o *Path) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetSourceReady

`func (o *Path) GetSourceReady() bool`

GetSourceReady returns the SourceReady field if non-nil, zero value otherwise.

### GetSourceReadyOk

`func (o *Path) GetSourceReadyOk() (*bool, bool)`

GetSourceReadyOk returns a tuple with the SourceReady field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSourceReady

`func (o *Path) SetSourceReady(v bool)`

SetSourceReady sets SourceReady field to given value.

### HasSourceReady

`func (o *Path) HasSourceReady() bool`

HasSourceReady returns a boolean if a field has been set.

### GetReaders

`func (o *Path) GetReaders() []PathReadersItem`

GetReaders returns the Readers field if non-nil, zero value otherwise.

### GetReadersOk

`func (o *Path) GetReadersOk() (*[]PathReadersItem, bool)`

GetReadersOk returns a tuple with the Readers field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReaders

`func (o *Path) SetReaders(v []PathReadersItem)`

SetReaders sets Readers field to given value.

### HasReaders

`func (o *Path) HasReaders() bool`

HasReaders returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


