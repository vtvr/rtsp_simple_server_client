# Conf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LogLevel** | Pointer to **string** |  | [optional] 
**LogDestinations** | Pointer to **[]string** |  | [optional] 
**LogFile** | Pointer to **string** |  | [optional] 
**ReadTimeout** | Pointer to **string** |  | [optional] 
**WriteTimeout** | Pointer to **string** |  | [optional] 
**ReadBufferCount** | Pointer to **int32** |  | [optional] 
**Api** | Pointer to **bool** |  | [optional] 
**ApiAddress** | Pointer to **string** |  | [optional] 
**Metrics** | Pointer to **bool** |  | [optional] 
**MetricsAddress** | Pointer to **string** |  | [optional] 
**Pprof** | Pointer to **bool** |  | [optional] 
**PprofAddress** | Pointer to **string** |  | [optional] 
**RunOnConnect** | Pointer to **string** |  | [optional] 
**RunOnConnectRestart** | Pointer to **bool** |  | [optional] 
**RtspDisable** | Pointer to **bool** |  | [optional] 
**Protocols** | Pointer to **[]string** |  | [optional] 
**Encryption** | Pointer to **string** |  | [optional] 
**RtspAddress** | Pointer to **string** |  | [optional] 
**RtspsAddress** | Pointer to **string** |  | [optional] 
**RtpAddress** | Pointer to **string** |  | [optional] 
**RtcpAddress** | Pointer to **string** |  | [optional] 
**MulticastIPRange** | Pointer to **string** |  | [optional] 
**MulticastRTPPort** | Pointer to **int32** |  | [optional] 
**MulticastRTCPPort** | Pointer to **int32** |  | [optional] 
**ServerKey** | Pointer to **string** |  | [optional] 
**ServerCert** | Pointer to **string** |  | [optional] 
**AuthMethods** | Pointer to **[]string** |  | [optional] 
**ReadBufferSize** | Pointer to **int32** |  | [optional] 
**RtmpDisable** | Pointer to **bool** |  | [optional] 
**RtmpAddress** | Pointer to **string** |  | [optional] 
**HlsDisable** | Pointer to **bool** |  | [optional] 
**HlsAddress** | Pointer to **string** |  | [optional] 
**HlsAlwaysRemux** | Pointer to **bool** |  | [optional] 
**HlsSegmentCount** | Pointer to **int32** |  | [optional] 
**HlsSegmentDuration** | Pointer to **string** |  | [optional] 
**HlsAllowOrigin** | Pointer to **string** |  | [optional] 
**Paths** | Pointer to [**map[string]PathConf**](PathConf.md) |  | [optional] 

## Methods

### NewConf

`func NewConf() *Conf`

NewConf instantiates a new Conf object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewConfWithDefaults

`func NewConfWithDefaults() *Conf`

NewConfWithDefaults instantiates a new Conf object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetLogLevel

`func (o *Conf) GetLogLevel() string`

GetLogLevel returns the LogLevel field if non-nil, zero value otherwise.

### GetLogLevelOk

`func (o *Conf) GetLogLevelOk() (*string, bool)`

GetLogLevelOk returns a tuple with the LogLevel field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLogLevel

`func (o *Conf) SetLogLevel(v string)`

SetLogLevel sets LogLevel field to given value.

### HasLogLevel

`func (o *Conf) HasLogLevel() bool`

HasLogLevel returns a boolean if a field has been set.

### GetLogDestinations

`func (o *Conf) GetLogDestinations() []string`

GetLogDestinations returns the LogDestinations field if non-nil, zero value otherwise.

### GetLogDestinationsOk

`func (o *Conf) GetLogDestinationsOk() (*[]string, bool)`

GetLogDestinationsOk returns a tuple with the LogDestinations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLogDestinations

`func (o *Conf) SetLogDestinations(v []string)`

SetLogDestinations sets LogDestinations field to given value.

### HasLogDestinations

`func (o *Conf) HasLogDestinations() bool`

HasLogDestinations returns a boolean if a field has been set.

### GetLogFile

`func (o *Conf) GetLogFile() string`

GetLogFile returns the LogFile field if non-nil, zero value otherwise.

### GetLogFileOk

`func (o *Conf) GetLogFileOk() (*string, bool)`

GetLogFileOk returns a tuple with the LogFile field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLogFile

`func (o *Conf) SetLogFile(v string)`

SetLogFile sets LogFile field to given value.

### HasLogFile

`func (o *Conf) HasLogFile() bool`

HasLogFile returns a boolean if a field has been set.

### GetReadTimeout

`func (o *Conf) GetReadTimeout() string`

GetReadTimeout returns the ReadTimeout field if non-nil, zero value otherwise.

### GetReadTimeoutOk

`func (o *Conf) GetReadTimeoutOk() (*string, bool)`

GetReadTimeoutOk returns a tuple with the ReadTimeout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadTimeout

`func (o *Conf) SetReadTimeout(v string)`

SetReadTimeout sets ReadTimeout field to given value.

### HasReadTimeout

`func (o *Conf) HasReadTimeout() bool`

HasReadTimeout returns a boolean if a field has been set.

### GetWriteTimeout

`func (o *Conf) GetWriteTimeout() string`

GetWriteTimeout returns the WriteTimeout field if non-nil, zero value otherwise.

### GetWriteTimeoutOk

`func (o *Conf) GetWriteTimeoutOk() (*string, bool)`

GetWriteTimeoutOk returns a tuple with the WriteTimeout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWriteTimeout

`func (o *Conf) SetWriteTimeout(v string)`

SetWriteTimeout sets WriteTimeout field to given value.

### HasWriteTimeout

`func (o *Conf) HasWriteTimeout() bool`

HasWriteTimeout returns a boolean if a field has been set.

### GetReadBufferCount

`func (o *Conf) GetReadBufferCount() int32`

GetReadBufferCount returns the ReadBufferCount field if non-nil, zero value otherwise.

### GetReadBufferCountOk

`func (o *Conf) GetReadBufferCountOk() (*int32, bool)`

GetReadBufferCountOk returns a tuple with the ReadBufferCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadBufferCount

`func (o *Conf) SetReadBufferCount(v int32)`

SetReadBufferCount sets ReadBufferCount field to given value.

### HasReadBufferCount

`func (o *Conf) HasReadBufferCount() bool`

HasReadBufferCount returns a boolean if a field has been set.

### GetApi

`func (o *Conf) GetApi() bool`

GetApi returns the Api field if non-nil, zero value otherwise.

### GetApiOk

`func (o *Conf) GetApiOk() (*bool, bool)`

GetApiOk returns a tuple with the Api field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApi

`func (o *Conf) SetApi(v bool)`

SetApi sets Api field to given value.

### HasApi

`func (o *Conf) HasApi() bool`

HasApi returns a boolean if a field has been set.

### GetApiAddress

`func (o *Conf) GetApiAddress() string`

GetApiAddress returns the ApiAddress field if non-nil, zero value otherwise.

### GetApiAddressOk

`func (o *Conf) GetApiAddressOk() (*string, bool)`

GetApiAddressOk returns a tuple with the ApiAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetApiAddress

`func (o *Conf) SetApiAddress(v string)`

SetApiAddress sets ApiAddress field to given value.

### HasApiAddress

`func (o *Conf) HasApiAddress() bool`

HasApiAddress returns a boolean if a field has been set.

### GetMetrics

`func (o *Conf) GetMetrics() bool`

GetMetrics returns the Metrics field if non-nil, zero value otherwise.

### GetMetricsOk

`func (o *Conf) GetMetricsOk() (*bool, bool)`

GetMetricsOk returns a tuple with the Metrics field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetrics

`func (o *Conf) SetMetrics(v bool)`

SetMetrics sets Metrics field to given value.

### HasMetrics

`func (o *Conf) HasMetrics() bool`

HasMetrics returns a boolean if a field has been set.

### GetMetricsAddress

`func (o *Conf) GetMetricsAddress() string`

GetMetricsAddress returns the MetricsAddress field if non-nil, zero value otherwise.

### GetMetricsAddressOk

`func (o *Conf) GetMetricsAddressOk() (*string, bool)`

GetMetricsAddressOk returns a tuple with the MetricsAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMetricsAddress

`func (o *Conf) SetMetricsAddress(v string)`

SetMetricsAddress sets MetricsAddress field to given value.

### HasMetricsAddress

`func (o *Conf) HasMetricsAddress() bool`

HasMetricsAddress returns a boolean if a field has been set.

### GetPprof

`func (o *Conf) GetPprof() bool`

GetPprof returns the Pprof field if non-nil, zero value otherwise.

### GetPprofOk

`func (o *Conf) GetPprofOk() (*bool, bool)`

GetPprofOk returns a tuple with the Pprof field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPprof

`func (o *Conf) SetPprof(v bool)`

SetPprof sets Pprof field to given value.

### HasPprof

`func (o *Conf) HasPprof() bool`

HasPprof returns a boolean if a field has been set.

### GetPprofAddress

`func (o *Conf) GetPprofAddress() string`

GetPprofAddress returns the PprofAddress field if non-nil, zero value otherwise.

### GetPprofAddressOk

`func (o *Conf) GetPprofAddressOk() (*string, bool)`

GetPprofAddressOk returns a tuple with the PprofAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPprofAddress

`func (o *Conf) SetPprofAddress(v string)`

SetPprofAddress sets PprofAddress field to given value.

### HasPprofAddress

`func (o *Conf) HasPprofAddress() bool`

HasPprofAddress returns a boolean if a field has been set.

### GetRunOnConnect

`func (o *Conf) GetRunOnConnect() string`

GetRunOnConnect returns the RunOnConnect field if non-nil, zero value otherwise.

### GetRunOnConnectOk

`func (o *Conf) GetRunOnConnectOk() (*string, bool)`

GetRunOnConnectOk returns a tuple with the RunOnConnect field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnConnect

`func (o *Conf) SetRunOnConnect(v string)`

SetRunOnConnect sets RunOnConnect field to given value.

### HasRunOnConnect

`func (o *Conf) HasRunOnConnect() bool`

HasRunOnConnect returns a boolean if a field has been set.

### GetRunOnConnectRestart

`func (o *Conf) GetRunOnConnectRestart() bool`

GetRunOnConnectRestart returns the RunOnConnectRestart field if non-nil, zero value otherwise.

### GetRunOnConnectRestartOk

`func (o *Conf) GetRunOnConnectRestartOk() (*bool, bool)`

GetRunOnConnectRestartOk returns a tuple with the RunOnConnectRestart field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRunOnConnectRestart

`func (o *Conf) SetRunOnConnectRestart(v bool)`

SetRunOnConnectRestart sets RunOnConnectRestart field to given value.

### HasRunOnConnectRestart

`func (o *Conf) HasRunOnConnectRestart() bool`

HasRunOnConnectRestart returns a boolean if a field has been set.

### GetRtspDisable

`func (o *Conf) GetRtspDisable() bool`

GetRtspDisable returns the RtspDisable field if non-nil, zero value otherwise.

### GetRtspDisableOk

`func (o *Conf) GetRtspDisableOk() (*bool, bool)`

GetRtspDisableOk returns a tuple with the RtspDisable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtspDisable

`func (o *Conf) SetRtspDisable(v bool)`

SetRtspDisable sets RtspDisable field to given value.

### HasRtspDisable

`func (o *Conf) HasRtspDisable() bool`

HasRtspDisable returns a boolean if a field has been set.

### GetProtocols

`func (o *Conf) GetProtocols() []string`

GetProtocols returns the Protocols field if non-nil, zero value otherwise.

### GetProtocolsOk

`func (o *Conf) GetProtocolsOk() (*[]string, bool)`

GetProtocolsOk returns a tuple with the Protocols field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtocols

`func (o *Conf) SetProtocols(v []string)`

SetProtocols sets Protocols field to given value.

### HasProtocols

`func (o *Conf) HasProtocols() bool`

HasProtocols returns a boolean if a field has been set.

### GetEncryption

`func (o *Conf) GetEncryption() string`

GetEncryption returns the Encryption field if non-nil, zero value otherwise.

### GetEncryptionOk

`func (o *Conf) GetEncryptionOk() (*string, bool)`

GetEncryptionOk returns a tuple with the Encryption field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEncryption

`func (o *Conf) SetEncryption(v string)`

SetEncryption sets Encryption field to given value.

### HasEncryption

`func (o *Conf) HasEncryption() bool`

HasEncryption returns a boolean if a field has been set.

### GetRtspAddress

`func (o *Conf) GetRtspAddress() string`

GetRtspAddress returns the RtspAddress field if non-nil, zero value otherwise.

### GetRtspAddressOk

`func (o *Conf) GetRtspAddressOk() (*string, bool)`

GetRtspAddressOk returns a tuple with the RtspAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtspAddress

`func (o *Conf) SetRtspAddress(v string)`

SetRtspAddress sets RtspAddress field to given value.

### HasRtspAddress

`func (o *Conf) HasRtspAddress() bool`

HasRtspAddress returns a boolean if a field has been set.

### GetRtspsAddress

`func (o *Conf) GetRtspsAddress() string`

GetRtspsAddress returns the RtspsAddress field if non-nil, zero value otherwise.

### GetRtspsAddressOk

`func (o *Conf) GetRtspsAddressOk() (*string, bool)`

GetRtspsAddressOk returns a tuple with the RtspsAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtspsAddress

`func (o *Conf) SetRtspsAddress(v string)`

SetRtspsAddress sets RtspsAddress field to given value.

### HasRtspsAddress

`func (o *Conf) HasRtspsAddress() bool`

HasRtspsAddress returns a boolean if a field has been set.

### GetRtpAddress

`func (o *Conf) GetRtpAddress() string`

GetRtpAddress returns the RtpAddress field if non-nil, zero value otherwise.

### GetRtpAddressOk

`func (o *Conf) GetRtpAddressOk() (*string, bool)`

GetRtpAddressOk returns a tuple with the RtpAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtpAddress

`func (o *Conf) SetRtpAddress(v string)`

SetRtpAddress sets RtpAddress field to given value.

### HasRtpAddress

`func (o *Conf) HasRtpAddress() bool`

HasRtpAddress returns a boolean if a field has been set.

### GetRtcpAddress

`func (o *Conf) GetRtcpAddress() string`

GetRtcpAddress returns the RtcpAddress field if non-nil, zero value otherwise.

### GetRtcpAddressOk

`func (o *Conf) GetRtcpAddressOk() (*string, bool)`

GetRtcpAddressOk returns a tuple with the RtcpAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtcpAddress

`func (o *Conf) SetRtcpAddress(v string)`

SetRtcpAddress sets RtcpAddress field to given value.

### HasRtcpAddress

`func (o *Conf) HasRtcpAddress() bool`

HasRtcpAddress returns a boolean if a field has been set.

### GetMulticastIPRange

`func (o *Conf) GetMulticastIPRange() string`

GetMulticastIPRange returns the MulticastIPRange field if non-nil, zero value otherwise.

### GetMulticastIPRangeOk

`func (o *Conf) GetMulticastIPRangeOk() (*string, bool)`

GetMulticastIPRangeOk returns a tuple with the MulticastIPRange field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMulticastIPRange

`func (o *Conf) SetMulticastIPRange(v string)`

SetMulticastIPRange sets MulticastIPRange field to given value.

### HasMulticastIPRange

`func (o *Conf) HasMulticastIPRange() bool`

HasMulticastIPRange returns a boolean if a field has been set.

### GetMulticastRTPPort

`func (o *Conf) GetMulticastRTPPort() int32`

GetMulticastRTPPort returns the MulticastRTPPort field if non-nil, zero value otherwise.

### GetMulticastRTPPortOk

`func (o *Conf) GetMulticastRTPPortOk() (*int32, bool)`

GetMulticastRTPPortOk returns a tuple with the MulticastRTPPort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMulticastRTPPort

`func (o *Conf) SetMulticastRTPPort(v int32)`

SetMulticastRTPPort sets MulticastRTPPort field to given value.

### HasMulticastRTPPort

`func (o *Conf) HasMulticastRTPPort() bool`

HasMulticastRTPPort returns a boolean if a field has been set.

### GetMulticastRTCPPort

`func (o *Conf) GetMulticastRTCPPort() int32`

GetMulticastRTCPPort returns the MulticastRTCPPort field if non-nil, zero value otherwise.

### GetMulticastRTCPPortOk

`func (o *Conf) GetMulticastRTCPPortOk() (*int32, bool)`

GetMulticastRTCPPortOk returns a tuple with the MulticastRTCPPort field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMulticastRTCPPort

`func (o *Conf) SetMulticastRTCPPort(v int32)`

SetMulticastRTCPPort sets MulticastRTCPPort field to given value.

### HasMulticastRTCPPort

`func (o *Conf) HasMulticastRTCPPort() bool`

HasMulticastRTCPPort returns a boolean if a field has been set.

### GetServerKey

`func (o *Conf) GetServerKey() string`

GetServerKey returns the ServerKey field if non-nil, zero value otherwise.

### GetServerKeyOk

`func (o *Conf) GetServerKeyOk() (*string, bool)`

GetServerKeyOk returns a tuple with the ServerKey field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetServerKey

`func (o *Conf) SetServerKey(v string)`

SetServerKey sets ServerKey field to given value.

### HasServerKey

`func (o *Conf) HasServerKey() bool`

HasServerKey returns a boolean if a field has been set.

### GetServerCert

`func (o *Conf) GetServerCert() string`

GetServerCert returns the ServerCert field if non-nil, zero value otherwise.

### GetServerCertOk

`func (o *Conf) GetServerCertOk() (*string, bool)`

GetServerCertOk returns a tuple with the ServerCert field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetServerCert

`func (o *Conf) SetServerCert(v string)`

SetServerCert sets ServerCert field to given value.

### HasServerCert

`func (o *Conf) HasServerCert() bool`

HasServerCert returns a boolean if a field has been set.

### GetAuthMethods

`func (o *Conf) GetAuthMethods() []string`

GetAuthMethods returns the AuthMethods field if non-nil, zero value otherwise.

### GetAuthMethodsOk

`func (o *Conf) GetAuthMethodsOk() (*[]string, bool)`

GetAuthMethodsOk returns a tuple with the AuthMethods field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAuthMethods

`func (o *Conf) SetAuthMethods(v []string)`

SetAuthMethods sets AuthMethods field to given value.

### HasAuthMethods

`func (o *Conf) HasAuthMethods() bool`

HasAuthMethods returns a boolean if a field has been set.

### GetReadBufferSize

`func (o *Conf) GetReadBufferSize() int32`

GetReadBufferSize returns the ReadBufferSize field if non-nil, zero value otherwise.

### GetReadBufferSizeOk

`func (o *Conf) GetReadBufferSizeOk() (*int32, bool)`

GetReadBufferSizeOk returns a tuple with the ReadBufferSize field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReadBufferSize

`func (o *Conf) SetReadBufferSize(v int32)`

SetReadBufferSize sets ReadBufferSize field to given value.

### HasReadBufferSize

`func (o *Conf) HasReadBufferSize() bool`

HasReadBufferSize returns a boolean if a field has been set.

### GetRtmpDisable

`func (o *Conf) GetRtmpDisable() bool`

GetRtmpDisable returns the RtmpDisable field if non-nil, zero value otherwise.

### GetRtmpDisableOk

`func (o *Conf) GetRtmpDisableOk() (*bool, bool)`

GetRtmpDisableOk returns a tuple with the RtmpDisable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtmpDisable

`func (o *Conf) SetRtmpDisable(v bool)`

SetRtmpDisable sets RtmpDisable field to given value.

### HasRtmpDisable

`func (o *Conf) HasRtmpDisable() bool`

HasRtmpDisable returns a boolean if a field has been set.

### GetRtmpAddress

`func (o *Conf) GetRtmpAddress() string`

GetRtmpAddress returns the RtmpAddress field if non-nil, zero value otherwise.

### GetRtmpAddressOk

`func (o *Conf) GetRtmpAddressOk() (*string, bool)`

GetRtmpAddressOk returns a tuple with the RtmpAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetRtmpAddress

`func (o *Conf) SetRtmpAddress(v string)`

SetRtmpAddress sets RtmpAddress field to given value.

### HasRtmpAddress

`func (o *Conf) HasRtmpAddress() bool`

HasRtmpAddress returns a boolean if a field has been set.

### GetHlsDisable

`func (o *Conf) GetHlsDisable() bool`

GetHlsDisable returns the HlsDisable field if non-nil, zero value otherwise.

### GetHlsDisableOk

`func (o *Conf) GetHlsDisableOk() (*bool, bool)`

GetHlsDisableOk returns a tuple with the HlsDisable field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHlsDisable

`func (o *Conf) SetHlsDisable(v bool)`

SetHlsDisable sets HlsDisable field to given value.

### HasHlsDisable

`func (o *Conf) HasHlsDisable() bool`

HasHlsDisable returns a boolean if a field has been set.

### GetHlsAddress

`func (o *Conf) GetHlsAddress() string`

GetHlsAddress returns the HlsAddress field if non-nil, zero value otherwise.

### GetHlsAddressOk

`func (o *Conf) GetHlsAddressOk() (*string, bool)`

GetHlsAddressOk returns a tuple with the HlsAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHlsAddress

`func (o *Conf) SetHlsAddress(v string)`

SetHlsAddress sets HlsAddress field to given value.

### HasHlsAddress

`func (o *Conf) HasHlsAddress() bool`

HasHlsAddress returns a boolean if a field has been set.

### GetHlsAlwaysRemux

`func (o *Conf) GetHlsAlwaysRemux() bool`

GetHlsAlwaysRemux returns the HlsAlwaysRemux field if non-nil, zero value otherwise.

### GetHlsAlwaysRemuxOk

`func (o *Conf) GetHlsAlwaysRemuxOk() (*bool, bool)`

GetHlsAlwaysRemuxOk returns a tuple with the HlsAlwaysRemux field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHlsAlwaysRemux

`func (o *Conf) SetHlsAlwaysRemux(v bool)`

SetHlsAlwaysRemux sets HlsAlwaysRemux field to given value.

### HasHlsAlwaysRemux

`func (o *Conf) HasHlsAlwaysRemux() bool`

HasHlsAlwaysRemux returns a boolean if a field has been set.

### GetHlsSegmentCount

`func (o *Conf) GetHlsSegmentCount() int32`

GetHlsSegmentCount returns the HlsSegmentCount field if non-nil, zero value otherwise.

### GetHlsSegmentCountOk

`func (o *Conf) GetHlsSegmentCountOk() (*int32, bool)`

GetHlsSegmentCountOk returns a tuple with the HlsSegmentCount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHlsSegmentCount

`func (o *Conf) SetHlsSegmentCount(v int32)`

SetHlsSegmentCount sets HlsSegmentCount field to given value.

### HasHlsSegmentCount

`func (o *Conf) HasHlsSegmentCount() bool`

HasHlsSegmentCount returns a boolean if a field has been set.

### GetHlsSegmentDuration

`func (o *Conf) GetHlsSegmentDuration() string`

GetHlsSegmentDuration returns the HlsSegmentDuration field if non-nil, zero value otherwise.

### GetHlsSegmentDurationOk

`func (o *Conf) GetHlsSegmentDurationOk() (*string, bool)`

GetHlsSegmentDurationOk returns a tuple with the HlsSegmentDuration field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHlsSegmentDuration

`func (o *Conf) SetHlsSegmentDuration(v string)`

SetHlsSegmentDuration sets HlsSegmentDuration field to given value.

### HasHlsSegmentDuration

`func (o *Conf) HasHlsSegmentDuration() bool`

HasHlsSegmentDuration returns a boolean if a field has been set.

### GetHlsAllowOrigin

`func (o *Conf) GetHlsAllowOrigin() string`

GetHlsAllowOrigin returns the HlsAllowOrigin field if non-nil, zero value otherwise.

### GetHlsAllowOriginOk

`func (o *Conf) GetHlsAllowOriginOk() (*string, bool)`

GetHlsAllowOriginOk returns a tuple with the HlsAllowOrigin field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHlsAllowOrigin

`func (o *Conf) SetHlsAllowOrigin(v string)`

SetHlsAllowOrigin sets HlsAllowOrigin field to given value.

### HasHlsAllowOrigin

`func (o *Conf) HasHlsAllowOrigin() bool`

HasHlsAllowOrigin returns a boolean if a field has been set.

### GetPaths

`func (o *Conf) GetPaths() map[string]PathConf`

GetPaths returns the Paths field if non-nil, zero value otherwise.

### GetPathsOk

`func (o *Conf) GetPathsOk() (*map[string]PathConf, bool)`

GetPathsOk returns a tuple with the Paths field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPaths

`func (o *Conf) SetPaths(v map[string]PathConf)`

SetPaths sets Paths field to given value.

### HasPaths

`func (o *Conf) HasPaths() bool`

HasPaths returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


