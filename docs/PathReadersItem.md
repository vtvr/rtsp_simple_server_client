# PathReadersItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | 
**Id** | Pointer to **string** |  | [optional] 

## Methods

### NewPathReadersItem

`func NewPathReadersItem(type_ string, ) *PathReadersItem`

NewPathReadersItem instantiates a new PathReadersItem object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPathReadersItemWithDefaults

`func NewPathReadersItemWithDefaults() *PathReadersItem`

NewPathReadersItemWithDefaults instantiates a new PathReadersItem object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *PathReadersItem) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *PathReadersItem) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *PathReadersItem) SetType(v string)`

SetType sets Type field to given value.


### GetId

`func (o *PathReadersItem) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *PathReadersItem) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *PathReadersItem) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *PathReadersItem) HasId() bool`

HasId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


